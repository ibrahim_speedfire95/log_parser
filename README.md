# README

## Setup

#### Install gems

```
bundle install
```

---

## How to use:

#### Task Script

```
./scripts/parser.rb file.log
```

#### Tests

```
rspec
```

#### Rubocop

```
rubocop
```

#### SimpleCov results

```
open coverage/index.html
```

---


## Potential improvements

- In `validate`, it can be more dynamic if we have more models and validations, like validations in rails (define validations for each attribute, and errors will be set dynamically).

- In `log_file_parser_service.spec`, it can use shared examples to take the `expect` block and expected output as parameters.

---

## Notes

- In validators, some parts like `instance_double` and stubs are not necessary, we can use string instead, but I did it like this to share my knowledge.

- I prefer to place the script code inside the script file in case it's a one-time script. but here I did it as a service because I thought about it like a script we can use it always.

- I added the `server.log` file to the project to test, but when I run the script I found that all IP addresses are invalid because I added more constraints to validate the IPv4:
  https://www.ibm.com/docs/en/ts3500-tape-library?topic=functionality-ipv4-ipv6-address-formats
  So I added another file `server2.log` and I updated some IPs and paths to cover all valid and invalid cases. 
