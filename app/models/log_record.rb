# frozen_string_literal: true

require_relative '../validators/ipv4_validator'
require_relative '../validators/request_path_validator'

# This model represents a log line
class LogRecord
  attr_reader :errors, :line, :path, :ipv4

  def initialize(attributes = {})
    @errors   = {}
    self.line = attributes[:line]
  end

  def line=(line)
    @line           = line
    @log_components = @line.nil? ? [] : @line.split
    @path           = @log_components[0]
    @ipv4           = @log_components[-1]
  end

  def valid?
    validate
    @errors.empty?
  end

  private

  def validate
    @errors        = {}
    @errors[:base] = 'invalide log record'   unless @log_components.length == 2
    @errors[:path] = 'invalide request path' unless RequestPathValidator.validate(@path)
    @errors[:ipv4] = 'invalide ipv4'         unless Ipv4Validator.validate(@ipv4)
  end
end
