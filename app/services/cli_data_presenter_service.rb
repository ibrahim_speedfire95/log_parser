# frozen_string_literal: true

require 'terminal-table'

# This service to print data in cli
class CliDataPresenterService
  def self.present(records)
    records.each do |record|
      send("print_#{record[:type]}", record[:data])
    end
  end

  def self.print_success(data)
    puts `tput setaf 2`
    puts data[:text]
    puts `tput sgr0`
  end

  def self.print_error(data)
    puts `tput setaf 1`
    puts data[:text]
    puts `tput sgr0`
  end

  def self.print_table(data)
    puts Terminal::Table.new(data)
  end
end
