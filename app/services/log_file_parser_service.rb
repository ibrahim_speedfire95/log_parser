# frozen_string_literal: true

require_relative '../models/log_record'
require_relative './log_file_reader_service'
require_relative './cli_data_presenter_service'

# This service to parse and analyze log file content
class LogFileParserService
  def initialize(file_path)
    @file_path           = file_path
    @valid_log_records   = []
    @invalid_log_records = []
  end

  def parse
    create_log_records
    print_data
  rescue StandardError => e
    CliDataPresenterService.present([{ type: :error, data: { text: e } }])
  end

  private

  def create_log_records
    @file_lines = LogFileReaderService.read(@file_path)
    @file_lines.each do |line|
      log_record = LogRecord.new(line: line)
      if log_record.valid?
        @valid_log_records << log_record
      else
        @invalid_log_records << log_record
      end
    end
  end

  def print_data
    data = most_pages_views + most_uniq_pages_views + log_errors
    CliDataPresenterService.present(data)
  end

  def most_pages_views
    page_views = Hash.new(0)
    @valid_log_records.each { |log_record| page_views[log_record.path] += 1 }

    [
      { type: :success, data: { text: 'Most pages views:' } },
      { type: :table,   data: { headings: %w[Page Visits], rows: format_page_views(page_views) } }
    ]
  end

  def most_uniq_pages_views
    page_views = Hash.new(0)
    @valid_log_records.uniq(&:line)
                      .each { |log_record| page_views[log_record.path] += 1 }

    [
      { type: :success, data: { text: 'Most unique page views:' } },
      { type: :table,   data: { headings: ['Page', 'Unique visits'], rows: format_page_views(page_views) } }
    ]
  end

  def format_page_views(page_views)
    page_views.sort_by { |_path, count| -count }
              .map     { |page_view| [page_view[0], "#{page_view[1]} #{page_view[1] == 1 ? 'visit' : 'visits'}"] }
  end

  def log_errors
    errors = Hash.new { |hash, key| hash[key] = [] }
    @invalid_log_records.each do |log_record|
      log_record.errors.each_value { |error| errors[error] << log_record.line }
    end

    errors.map do |error, logs|
      [
        { type: :error, data: { text: "#{error.capitalize}:" } },
        { type: :table, data: { headings: ['Lines'], rows: logs.each_slice(1).to_a } }
      ]
    end.flatten
  end
end
