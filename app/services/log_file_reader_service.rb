# frozen_string_literal: true

# This service to help with reading .log files
class LogFileReaderService
  def self.read(file_path)
    raise 'The file path is missing' if     file_path.nil?
    raise "The file doesn't exist"   unless File.file?(file_path)

    File.read(file_path).split("\n")
  end
end
