# frozen_string_literal: true

# Validator to validate IPv4
class Ipv4Validator
  IPV4_OCTET_REGEX = /([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])/.freeze
  IPV4_REGEX       = /^#{IPV4_OCTET_REGEX}(\.#{IPV4_OCTET_REGEX}){3}$/.freeze

  def self.validate(string)
    string.match?(IPV4_REGEX)
  end
end
