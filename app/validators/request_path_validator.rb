# frozen_string_literal: true

# Validator to validate request path
class RequestPathValidator
  def self.validate(string)
    uri = URI.parse(string)

    string[0] == '/' && uri.scheme.nil? && uri.host.nil? && !uri.path.nil?
  rescue URI::InvalidURIError
    false
  end
end
