#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../app/services/log_file_parser_service'

LogFileParserService.new(ARGV[0]).parse
