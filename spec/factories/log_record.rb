# frozen_string_literal: true

FactoryBot.define do
  factory :log_record do
    line { '/home 192.168.1.1' }
  end
end
