# frozen_string_literal: true

require_relative '../../app/models/log_record'
require_relative './shared_examples/invalid_record'

RSpec.describe LogRecord do
  describe '#valid?' do
    context 'valid log record' do
      subject { build(:log_record) }

      it 'should return true if the log record is valid' do
        expect(subject.valid?).to be true
      end
    end

    context 'invalid log record' do
      subject { build(:log_record, line: '/home 2022-01-29 23:25:16 +0200 192.168.1.1') }
      include_examples 'invalid record'
    end

    context 'invalid path' do
      subject { build(:log_record, line: 'home 192.168.1.18') }
      include_examples 'invalid record'
    end

    context 'invalid ipv4' do
      subject { build(:log_record, line: '/home 184.123.665.067') }
      include_examples 'invalid record'
    end
  end
end
