# frozen_string_literal: true

RSpec.shared_examples 'invalid record' do
  it 'should return false if the record is invalid' do
    expect(subject.valid?).to be false
  end
end
