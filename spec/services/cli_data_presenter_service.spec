# frozen_string_literal: true

require_relative '../../app/services/cli_data_presenter_service'

RSpec.describe CliDataPresenterService do
  describe '.present' do
    it 'should print the data in cli' do
      data = [
        { type: :success, data: { text: 'ok' } },
        { type: :error,   data: { text: 'err' } },
        { type: :table,   data: { headings: ['#'], rows: [[1]] } }
      ]

      expected_output = "\e[32m\nok\n\e(B\e[m\n\e[31m\nerr\n\e(B\e[m\n+---+\n| # |\n+---+\n| 1 |\n+---+\n"
      expect { described_class.present(data) }.to output(expected_output).to_stdout
    end
  end
end
