# frozen_string_literal: true

require_relative '../../app/services/log_file_parser_service'

RSpec.describe LogFileParserService do
  describe '#parse' do
    context 'valid file path' do
      it 'should parse and analyze log file content and print the result in cli' do
        file_path       = './spec/fixtures/files/log_file_parser.log'
        expected_output = "\e[32m\nMost pages views:\n\e(B\e[m\n" \
                          "+----------+----------+\n" \
                          "| Page     | Visits   |\n" \
                          "+----------+----------+\n" \
                          "| /contact | 2 visits |\n" \
                          "| /home    | 2 visits |\n" \
                          "+----------+----------+\n" \
                          "\e[32m\nMost unique page views:\n\e(B\e[m\n" \
                          "+----------+---------------+\n" \
                          "| Page     | Unique visits |\n" \
                          "+----------+---------------+\n" \
                          "| /home    | 2 visits      |\n" \
                          "| /contact | 1 visit       |\n" \
                          "+----------+---------------+\n" \
                          "\e[31m\nInvalide request path:\n\e(B\e[m\n" \
                          "+-------------------+\n" \
                          "| Lines             |\n" \
                          "+-------------------+\n" \
                          "| home 192.168.1.18 |\n" \
                          "+-------------------+\n" \
                          "\e[31m\nInvalide ipv4:\n\e(B\e[m\n" \
                          "+------------------------+\n" \
                          "| Lines                  |\n" \
                          "+------------------------+\n" \
                          "| /about 897.280.786.156 |\n" \
                          "+------------------------+\n" \
                          "\e[31m\nInvalide log record:\n\e(B\e[m\n" \
                          "+-----------------------+\n" \
                          "| Lines                 |\n" \
                          "+-----------------------+\n" \
                          "| /about s 192.168.1.21 |\n" \
                          "+-----------------------+\n"

        expect { described_class.new(file_path).parse }.to output(expected_output).to_stdout
      end
    end

    context 'missing file path' do
      it 'should print error in cli' do
        expected_output = "\e[31m\nThe file path is missing\n\e(B\e[m\n"
        expect { described_class.new(nil).parse }.to output(expected_output).to_stdout
      end
    end

    context 'invalid file path' do
      it 'should print error in cli' do
        expected_output = "\e[31m\nThe file doesn't exist\n\e(B\e[m\n"
        expect { described_class.new('./non_exist.log').parse }.to output(expected_output).to_stdout
      end
    end
  end
end
