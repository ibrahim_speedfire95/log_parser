# frozen_string_literal: true

require_relative '../../app/services/log_file_reader_service'

RSpec.describe LogFileReaderService do
  describe '.read' do
    context 'valid file path' do
      it 'should return the log file lines in an array if the file path is valid and the file exists' do
        log          = described_class.read('./spec/fixtures/files/log_file_reader.log')
        expected_log = [
          '/home 184.123.665.067',
          '/about/2 444.701.448.104',
          '/help_page/1 929.398.951.889'
        ]

        expect(log).to match_array expected_log
      end
    end

    context 'missing file path' do
      it 'should raise an error if the file path is missing' do
        expect { described_class.read(nil) }.to raise_error(/The file path is missing/)
      end
    end

    context 'invalid file path' do
      it 'should raise an error if the file path is invalid' do
        expect { described_class.read('./non_exist.log') }.to raise_error(/The file doesn't exist/)
      end
    end
  end
end
