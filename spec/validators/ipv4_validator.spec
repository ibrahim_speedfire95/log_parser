# frozen_string_literal: true

require_relative '../../app/validators/ipv4_validator'

RSpec.describe Ipv4Validator do
  describe '.validate' do
    context 'valid IPv4' do
      it 'should return true if the ip is valid' do
        ipv4s = [
          '192.168.1.1',
          '0.0.0.0',
          '255.255.255.255'
        ]

        ipv4s.each do |ipv4|
          log_record = instance_double('LogRecord', ipv4: ipv4)
          expect(described_class.validate(log_record.ipv4)).to be true
        end
      end
    end

    context 'invalid IPv4' do
      it 'should return false if the ip is invalid' do
        ipv4s = [
          '256.256.256.256',
          '1.2.3',
          '127.00.0.1'
        ]

        ipv4s.each do |ipv4|
          log_record = instance_double('LogRecord', ipv4: ipv4)
          expect(described_class.validate(log_record.ipv4)).to be false
        end
      end
    end
  end
end
