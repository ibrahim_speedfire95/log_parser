# frozen_string_literal: true

require_relative '../../app/validators/request_path_validator'

RSpec.describe RequestPathValidator do
  describe '.validate' do
    context 'valid request path' do
      it 'should return true if the path is valid' do
        paths = [
          '/home',
          '/about/2',
          '/help_page/1'
        ]

        paths.each do |path|
          path_record = double('path_record')
          allow(path_record).to receive(:path) { path }
          expect(described_class.validate(path_record.path)).to be true
        end
      end
    end

    context 'invalid request path' do
      it 'should return false if the path is invalid' do
        paths = [
          'https://www.example.com/home',
          'example.com/about/2',
          '/help_page/ 1'
        ]

        paths.each do |path|
          path_record = double('path_record')
          allow(path_record).to receive(:path) { path }
          expect(described_class.validate(path_record.path)).to be false
        end
      end
    end
  end
end
